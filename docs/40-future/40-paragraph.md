---
title: Other paragraph definitions
layout: default
nav_order: 40
parent: Future Improvement Ideas
permalink: /future-improvement-ideas/paragraph.html
---

# Other paragraph definitions

We could experiment with another possible definition of *paragraph*:
text delimited by empty lines or the shell prompt. (Requires, of course,
a shell integration extension that makes the terminal aware of the shell
prompt.) This formatting is often used in longer text files such as
readme files, licenses, markdown documents and such. (Is this an often
used format in RTL languages, though? And how much would we want to go
into the business of recognizing other formatting elements, let's say
bullet points of lists? Is it fair to say that handling such formats is
no longer the terminal's job but belongs to applications?)

Independently to this, as mentioned in the subsection "Autodetecting the
direction", autodetecting could also look at subsequent paragraphs if we
know the boundaries between commands' output.
