---
title: Emulated explicit mode
layout: default
nav_order: 10
parent: Rationale and Dropped Ideas
permalink: /rationale-and-dropped-ideas/emulated-explicit-mode.html
---

# Emulated explicit mode

Strictly speaking, if implicit mode level 2 gets designed and
implemented, explicit mode becomes superfluous. One could emulate
explicit mode by embedding each paragraph in LRO...PDF or RLO...PDF.

This would still be a nasty hack, though.

It would complicate the way BiDi-aware apps have to make sure that those
BiDi override characters get always preserved across partial or full
updates of the screen. Apps that paint the canvas on their own could do
it (but probably wouldn't want to), apps that rely on existing screen
handling libraries just could not (until the library adds support).
Also, this "emulated explicit" mode couldn't be used when explicit mode
is used for "damage control" in BiDi-unaware apps.

Copy-pasting would perhaps also include these characters which would be
unexpected.

I found it significantly cleaner to introduce (adapt from TR/53)
explicit mode.
