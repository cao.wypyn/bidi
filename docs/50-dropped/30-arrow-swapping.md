---
title: More arrow key swapping modes
layout: default
nav_order: 30
parent: Rationale and Dropped Ideas
permalink: /rationale-and-dropped-ideas/more-arrow-key-swapping-modes.html
---

# More arrow key swapping modes

Strictly speaking, the proposed design with arrow key swapping modes
isn't entirely symmetrical.

In an LTR paragraph, toggling between the two modes doesn't cause any
change. In an RTL paragraph, toggling between the two swaps them.

Let's take two utilities that each take some time to complete, and one
prefers one mode while the other prefers the other mode. These two are
run after each other from a shell script. While the first utility is
still running, the user types ahead an arrow key that will be handled by
the second utility. In LTR this works as expected; in RTL the cursor
ends up walking in the unexpected direction.

A mode that unconditionally swaps the two arrow keys wouldn't suffer
from such a problem, but would be impossible to hook up to be executed
at the proper times.

A fully symmetrical approach would require either at least 3 modes, plus
distinct keycodes for the auto-swapping behavior (that is, to separate
*logical* from *visual* behaviors) which would be problematic with the
input handling of most apps; or would require 4 modes if we wished to
stick to the existing keycodes.

It feels an overkill to me for such a negligible corner case, a design
that probably no application would care to use properly. Here I'd
sacrifice the theoretically perfect design for a practical one.
