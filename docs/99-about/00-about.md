---
title: About
layout: default
nav_order: 99
permalink: /about.html
---

## Author, license

This work is © 2018–2019 Egmont Koblinger.

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img
alt="Creative Commons License" style="border-width:0"
src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This
work is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by/4.0/">Creative Commons
Attribution 4.0 International License</a>.

The RTL Terminal icon is the mirrored variant of the one found in the
Humanity Icon Theme in Ubuntu 18.04 (package: humanity-icon-theme, file:
/usr/share/icons/Humanity/apps/128/utilities-terminal.svg), licensed as [GPL
v2](LICENSE-GPL2.txt).

The site uses a slightly modified version of the [Just the
Docs](https://github.com/pmarsceill/just-the-docs) template for Jekyll,
Copyright © 2016 Patrick Marsceill, licensed as [MIT](LICENSE-MIT.txt).

## Feedback

You can send feedback by [opening a new
issue](https://gitlab.freedesktop.org/terminal-wg/bidi/issues).

## Changes

- v0.1, 2019-01-29

  Initial publication
