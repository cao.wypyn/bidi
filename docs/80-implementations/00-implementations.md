---
title: Implementations
layout: default
nav_order: 80
permalink: /implementations.html
---

# Implementations
{:.no_toc}

Here I'm planning to maintain a list of implementations that are known
to support BiDi according to this specification.

I do not intend to maintain a list of those that do some BiDi, but not
in accordance with this specs. See [XVilka's
site](https://gist.github.com/XVilka/a0e49e1c65370ba11c17) for such an
effort.

* TOC
{:toc}

## VTE

VTE is the terminal emulation widget behind GNOME Terminal and a bunch
of other terminal emulator applications.

BiDi support according to this specification (level 1 only for the time
being) is planned to be added to VTE version 0.58, released along with
GNOME 3.34 in September 2019\.

The current work-in-progress implementation is quite robust and
showcases the features pretty well. There are still some bugs to fix and
lots of code cleanup to do.

The work-in-progress implementation resides in the `wip/egmont/bidi`
branch. To give it a try, install the standard GTK+ development packages
as well as fribidi-1.0, and follow these steps:

    git clone https://gitlab.gnome.org/GNOME/vte.git
    cd vte
    git checkout wip/egmont/bidi
    ./autogen.sh --enable-debug
    make
    ./src/app/vte-2.91

or optionally

    VTE_DEBUG=bidi ./src/app/vte-2.91

to highlight on the screen every character that has RTL resolved
directionality, plus get some debug info printed as well.

See the file `BIDI-STATUS` inside the repository for what's already
supported and what's not, plus a few convenience aliases.

Make sure to check out this cool test: `cat doc/bidi.txt`.
