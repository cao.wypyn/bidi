---
title: Applications
layout: default
nav_order: 30
parent: Prior Work
permalink: /prior-work/applications.html
---

# Applications
{:.no_toc}

* TOC
{:toc}

## Emacs

Note that I'm talking about the GNU Emacs text editor running inside a
terminal, and *not* the terminal emulator contained within Emacs.

I haven't studied its possible configurations, only the default provided
by GNU Emacs-25.2 on Ubuntu 18.10.

It does BiDi on its own, hence with a BiDi-aware terminal emulator RTL
strings are reversed back to the incorrect order. The terminal emulator
needs to switch to explicit mode. Hopefully Emacs will be adjusted to
emit the corresponding escape sequences; until then a wrapper script or
a terminfo entry could provide a workaround.

It first runs BiDi on a complete line of a file, shuffles and mirrors
the characters accordingly, but then wraps into lines of the terminal
emulator as if it was LTR only. This is at least unusual, contrary to
UAX \#9, and thus the eye sometimes has to move upwards to the previous
row of the terminal emulator while reading.

It does not do Arabic shaping.

## Vim, NeoVim

I haven't studied their possible configurations, only the default
provided by Vim 8.0.1766 and Nvim 0.3.1 on Ubuntu 18.10.

They don't do BiDi, however, do Arabic shaping. This combination really
doesn't make sense to me.

Even worse, they run the shaping algorithm on the reverse (LTR) order.
The logical first letter of a word is shaped to its "final" form, as if
it was connected towards its right, that is, as if it was supposed to
appear at the left end of a word; and vice versa. No matter if the
terminal emulator does BiDi or not, the result is broken.
